$(eval $(call BeesipDefaults,x86_64))
$(eval $(call BeesipDefaults,virtual))
$(eval $(call BeesipDefaults,honey))

TARGET_CPU=x86
TARGET_NAME=honey_virtual-$(BEESIP_VERSION)-$(TARGET_CPU)

BEESIP_PACKAGES+=

OWRT_NAME=chaos_calmer
OWRT_IMG_PROFILE=Generic
OWRT_CONFIG_SET+=TARGET_KERNEL_PARTSIZE=30 TARGET_ROOTFS_PARTSIZE=300

OWRT_ADDED_FILES+=virtual-files/network.uci:/etc/config/network virtual-files/dhcp.uci:/etc/config/dhcp
