$(eval $(call BeesipDefaults,x86_64))
$(eval $(call BeesipDefaults,virtual))
$(eval $(call BeesipDefaults,callmon))

TARGET_CPU=x86

OWRT_NAME=chaos_calmer

TARGET_NAME=callmon-$(BEESIP_VERSION)-virtual_$(TARGET_CPU)

# Options for qemu emulation of target
TARGET_QEMU=i386
TARGET_QEMU_OPTS=-m 512

# ImageBuilder profile for your platform
OWRT_IMG_PROFILE=Generic

# More packages to include into image
BEESIP_PACKAGES+=tcpdump=y

# Which config settings of OpenWrt should be set to .config
OWRT_CONFIG_SET+=TARGET_KERNEL_PARTSIZE=30

# Which files to manualy add to filesystem just during build (from:to)
OWRT_ADDED_FILES+=virtual-files/network.uci:/etc/config/network virtual-files/dhcp.uci:/etc/config/dhcp
