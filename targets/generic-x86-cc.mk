$(eval $(call BeesipDefaults,x86_32))
$(eval $(call BeesipDefaults,generic))

#
# OpenWrt used for compilation (trunk, attitude_adjustment, ...)
#

TARGET_CPU=x86
OWRT_NAME=chaos_calmer

#
# Will be used as a prefix for various images
#

TARGET_NAME=pc_$(BEESIP_VERSION)-$(TARGET_CPU)

# Name of images generated by OpenWrt build
# Some platforms supports some types of image
# Comment images which are not suitable for your platform
# Disk image (can be directly booted or flashed)
# You can optionally fix the images filename.

#OWRT_IMG_DISK_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-combined-squashfs.img
#OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-vmlinuz
#OWRT_IMG_SQUASHFS_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-rootfs-squashfs.img

#
# ImageBuilder profile for your platform
#

OWRT_IMG_PROFILE=Generic

#
# More packages to include into image
#

BEESIP_PACKAGES += callmon-probe=y curl=y

#
# Which config settings of OpenWrt should be set to .config
#

OWRT_CONFIG_SET += TARGET_x86=y TARGET_BOARD=\"x86\" HAS_SUBTARGETS=y TARGET_KERNEL_PARTSIZE=30 TARGET_ROOTFS_PARTSIZE=300 TARGET_OVERLAY_JFFS2=y

#
# Which OpenWrt settings should be explicitly unset
#

#
# Add external files repository
#

$(eval $(call AddExternalFilesRepo,https://bitbucket.org/liptel/customrepository1.git))

OWRT_ADDED_EXTERNAL_FILES = README.md:/etc/README.md

#
# Add custom package repository
#

$(eval $(call AddOpenWrtFeed,src-git luci2 git://git.openwrt.org/project/luci2/ui.git))
