TARGET_CPU=ar71xx
OWRT_NAME=chaos_calmer
BEESIP_NAME=chaos_calmer

TARGET_NAME=eduroamaplte_$(HWREV)_$(BEESIP_VERSION)-owrt_$(OWRT_NAME)

HWREV=wdr3600-v1

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

$(eval $(call BeesipDefaults,eduroam))

BEESIP_PACKAGES += \
	lldpd=n tcpdump=n tcpdump-mini=n dnsmasq=n \
	tftp-hpa=n ip=n odhcp6c=n odhcpd=n kmod-ipv6=n \
	wpad=y hostapd=n firewall=n syslog-ng3=n \
	openvpn-openssl=y openssl-util=y \
	iftop=n scdp=n eapol-test=n flow-tools=n \
	iptables=n ip6tables=n firewall=n luci-app-mwan3=n \
	luci-app-firewall=n luci=n freifunk-firewall=n \
	freifunk-gwcheck=n luci-mod-freifunk=n \
	luci-app-p2pblock=n luci-app-multiwan=n \
	meshwizard=n luci-mod-freifunk-community=n \
	picocom=y kmod-usb-net-cdc-ether=y kmod-usb-net-cdc-ncm=y \
	kmod-usb-net-huawei-cdc-ncm=y kmod-usb-net-qmi-wwan=y \
	usb-modeswitch=y kmod-mii=y kmod-usb-net=y kmod-usb-wdm=y uqmi=y \
	kmod-usb-serial-option=y kmod-usb-serial=y kmod-usb-serial-wwan=y \
	comgt=y comgt-ncm=y ppp=y ppp-mod-pppoe=y kmod-usb-net-rndis=y \
	kmod-ppp=y kmod-pppoe=y kmod-pppox=y kmod-slhc=y

OWRT_CONFIG_UNSET += \
	PACKAGE_firewall PACKAGE_luci-app-mwan3 PACKAGE_luci-app-firewall \
	PACKAGE_luci PACKAGE_freifunk-firewall PACKAGE_freifunk-gwcheck \
	PACKAGE_luci-mod-freifunk PACKAGE_luci-app-p2pblock PACKAGE_luci-app-multiwan \
	PACKAGE_meshwizard PACKAGE_luci-mod-freifunk-community \
	DEFAULT_dnsmasq DEFAULT_iptables DEFAULT_ip6tables DEFAULT_odhcp6c DEFAULT_odhcpd \
	DEFAULT_wpad-mini

OWRT_IMG_BIN_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-factory.bin
OWRT_IMG_PROFILE=TLWDR4300
OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-vmlinux.elf

OWRT_CONFIG_SET += TARGET_ar71xx=y TARGET_ar71xx_generic_TLWDR4300=y
