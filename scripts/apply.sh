#!/bin/sh

BEESIPDIR="$1"; shift
STAMPDIR="$1"; shift

mkstamp () {
  echo $i | tr '/ .' '___'
}

case $1 in 
"patch")
  shift;
  for i in $*; do
    stamp=$(mkstamp "$i") 
    if ! [ -f "$STAMPDIR/$stamp" ]; then
      echo "BEESIP PATCH $BEESIPDIR/patches/$i ($STAMPDIR/$stamp does not exists)" >&2
      patch -p0 <$BEESIPDIR/patches/$i
      touch "$STAMPDIR/$stamp"
      echo "-----"
    else
      echo "$i already patched, skipping patch"
    fi
  done
  ;;
"patch-dir")
  shift;
  [ -d $1 ] || exit 0
  cd $1
  for feed in *; do
	(cd $feed
        for patch in *; do
                (cd ../../feeds/$feed
		echo $PWD $feed $patch
                patch -f -p1 < ../../patches/$feed/$patch
                )
        done
        )
  done
  ;;
"unpatch")
  shift;
  for i in $*; do 
    stamp=$(mkstamp "$i") 
    if [ -f "$STAMPDIR/$stamp" ]; then
      echo "BEESIP UNPATCH $BEESIPDIR/patches/$i ($STAMPDIR/$stamp exists)" >&2
      patch -R -p0 <$BEESIPDIR/patches/$i
      rm -f "$STAMPDIR/$stamp"
    else
      echo "$i not patched, skipping unpatch"
    fi
  done
  ;;
"patch-trunk"|"patch-attitude_adjustment"|"patch-barrier_breaker"|"patch-chaos_calmer"|"patch-designated_driver")
  suite=$(echo $1 | cut -d '-' -f 2)
  for i in $BEESIPDIR/patches/$suite/*patch $BEESIPDIR/patches/$suite/*diff ; do
    stamp=$(mkstamp "$i") 
    if ! [ -f "$STAMPDIR/$stamp" ]; then
      echo "BEESIP PATCH($suite) $i ($STAMPDIR/$stamp does not exists)" >&2
      patch -p0 <$i
      touch "$STAMPDIR/$stamp"
      echo "-----"
    else
      echo "$i already patched, skipping patch"
    fi
  done
  ;;
"unpatch-trunk"|"unpatch-attitude_adjustment"|"unpatch-barrier_breaker"|"unpatch-chaos_calmer"|"patch-designated_driver")
  suite=$(echo $1 | cut -d '-' -f 2)
  for i in $BEESIPDIR/patches/$suite/*patch $BEESIPDIR/patches/$suite/*diff; do
    stamp=$(mkstamp "$i") 
    if [ -f "$STAMPDIR/$stamp" ]; then
      echo "BEESIP UNPATCH $i ($STAMPDIR/$stamp exists)" >&2
      patch -R -p0 <$i
      rm -f "$STAMPDIR/$stamp"
    else
      echo "$i not patched, skipping unpatch"
    fi
  done
  ;;
esac

