#!/bin/sh

uciprov_initmodule2 mgmt

mgmt_passwd(){
      (echo "$1"; sleep 1; echo "$1") | passwd $2
}

mgmt_setrootpw(){
      local pw
      if [ "$set_root_pw" = "1" ]; then
        pw=$(gen_rand_txt 20)
        if [ -n "$save_pw" ]; then 
	  echo "$pw" >$save_pw
          mgmt_passwd "$pw" root
          mblog mgmt "Password for root was set and saved to $savepw."
	fi
      else
        true
      fi
}

uciprov_mgmt(){
	local uri=$(uciprov_runmodule2 "$@") || return 1;
	local set_root_pw
	local authorized_keys
	local save_pw
	local disable_ssh_pwauth
	
	config_load uciprov
	config_get set_root_pw mgmt set_root_pw
	config_get authorized_keys mgmt authorized_keys
	config_get save_pw mgmt save_pw
	
	if [ -n "$uri" ];
	then
		 fetch_uri "$uri" >$UCITMP/mgmt$$
		 md5_new=$(md5sum $UCITMP/mgmt$$ | cut -d ' ' -f 1)
		 md5_old=$(md5sum $authorized_keys | cut -d ' ' -f 1)
		 if [ "$md5_new" != "$md5_old" ]; then 
		   cp $UCITMP/mgmt$$ "$authorized_keys";
		   mblog mgmt "Fetched authorized keys to $authorized_keys."
		 fi
		 if [ -n "$save_pw" ] && ! [ -f "$save_pw" ]; then 
		 	mgmt_setrootpw
		 fi
		 uciprov_donemodule2 mgmt "$md5"
		 rm -f $UCITMP/mgmt$$
	fi
}

mgmt_help(){
	echo "Uciprov management module to fetch authorized SSH keys and set root password."
}


