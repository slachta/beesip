#!/bin/sh

uciprov_initmodule2 ssldeploy openssl

uciprov_ssldeploy(){
	local uri=$(uciprov_runmodule2 "$@") || return 1;
	local algo bits subject keyfile crtfile keyowner keychmod fetchuri pushuri muri
	config_load uciprov
	config_get algo ssldeploy algo
	config_get bits ssldeploy bits
	config_get subject ssldeploy subject
	subject=$(prepare_uri $subject)
	config_get keyfile ssldeploy keyfile
	config_get crtfile ssldeploy crtfile
	config_get csrfile ssldeploy csrfile
	config_get keyowner ssldeploy keyowner
	config_get keychmod ssldeploy keychmod
	config_get fetchuri ssldeploy fetch_uri
	config_get pushuri ssldeploy push_uri

	[ -f "$crtfile" ] && mblog ssldeploy "Already deployed $crtfile." && return

	if ! [ -f "$keyfile" ]; then
	  mdbg 3 ssldeploy "Generating SSL certificate (openssl req -nodes -newkey $algo:$bits -keyout $keyfile -out $csrfile -nodes -subj /\CN=$subject/)"
	  openssl req -nodes -newkey "$algo:$bits" -keyout "$keyfile" -out "$csrfile" -nodes -subj "/\CN=$subject/" >/tmp/uciprov/ssldeploy.log 2>&1 && \
	  chown $keyowner $keyfile && chmod $keychmod $keyfile
	fi

	if [ -f "$keyfile" ]; then
	  puri=$(prepare_uri $pushuri)
	  furi=$(prepare_uri $fetchuri)
	  if push_uri "$csrfile" "$puri" "text/x-pem-file"; then
	    mblog ssldeploy "Pushed $csrfile to $puri."
	  else
	    mblog ssldeploy "$puri not reachable! Cannot push csr request!"
	  fi
	  validate_uri "$furi" && fetch_uri "$furi" >$crtfile && chmod 444 $crtfile && mblog ssldeploy "Delivered $crtfile." || mblog ssldeploy "Waiting for cert file at $furi"
	else
	  mblog ssldeploy "Cannot generate $keyfile. Check perms and openssl command."
	fi
}

ssldeploy_help(){
	echo "Uciprov SSL deploy module"
}

