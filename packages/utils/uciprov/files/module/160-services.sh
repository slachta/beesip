#!/bin/sh

uciprov_initmodule2 services

uciprov_services(){
	local uri=$(uciprov_runmodule2 "$@") || return 1;
	config_load uciprov
	fetch_uri "$uri" >$UCITMP/services || return 1;
	grep -v '^#' $UCITMP/services | while read service action condition rest; do
	  [ -z "$service" ] && continue;
	  [ -x "/etc/init.d/$service" ] || continue;
	  mdbg 2 services "/etc/init.d/$service" "$action"
	  "/etc/init.d/$service" "$action"
	done
}

services_help(){
	echo "Uciprov services module"
}
