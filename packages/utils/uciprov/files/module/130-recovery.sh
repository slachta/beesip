#!/bin/sh

uciprov_initmodule2 recovery

uciprov_recovery(){
	local uri=$(uciprov_runmodule2 "$@") || return 1;
	local flag=$(fetch_uri "$uri")
	config_load uciprov
	local fd_flag
	config_get fd_flag recovery fd_flag
	
	if [ -n "$uri" ] && ( [ "$flag" = "$fd_flag" ] );
	then
	    uciprov_fd
	else
	    mdbg 3 recovery "Cfgflag=$fd_flag, Got '$flag'"
	fi
	recovery_dhcp
}

recovery_help(){
	echo "Uciprov recovery module"
}

recovery_sh(){
	config_load uciprov
	config_get shcmd "recovery" script
	[ -n "$shcmd" ] && $shcmd
}

recovery_dhcp(){
	config_load uciprov
	config_get enabled "recovery" dhcp
	! [ "$enabled" = "1" ] && return
	recurl=$(get_dhcp_var $interface boot_file)
	if echo $recurl | grep -q 'uci.default$';
	then
	    mblog recovery "Using defaults uci from $recurl and rebooting!"
	    fetch_uri $recurl | uci import
	    uci commit
	    /sbin/reboot -f
	fi
	
	if echo $recurl | grep -q 'recovery.img$';
	then
	      mblog recovery "Using recovery image from $recurl and doing sysupgrade!"
	      fetch_uri $recurl >/tmp/recovery.img
	      sysupgrade -v -n /tmp/recovery.img || mblog recovery "Error during sysupgrade!"
	fi
}

uci_recovery(){
	recovery_dhcp
	recovery_sh
}

