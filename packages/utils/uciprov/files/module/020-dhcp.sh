#!/bin/sh

. /usr/share/libubox/jshn.sh

if uciprov_initmodule1 dhcp; then
  if [ -n "$interface" ]; then
    uciprov_hook_add uciprov_geturi get_uri_dhcp
  else
    interface=wan
  fi
fi

get_dhcp_var(){
	local var
	local isup
	local proto
	local data=$(ubus -S call "network.interface.$1" status)
	
	json_load "$data" && \
	  json_get_var isup up && \
	  json_get_var proto proto && \
	  [ "$proto" = "dhcp" ] && \
	  [ "$isup" = "1" ] && {
	   json_select data >/dev/null && \
		json_get_var var "lease_$2"	# Old style data
	   	[ -z "$var" ] &&  json_get_var var "$2"	# New style data (chaos_calmer)
	   [ "$2" = "domain" ] && [ -z "$var" ] && json_load "$data" && \
	     json_select dns-search && json_get_var var 1
	   echo $var
	  }
}

get_uri_dhcp(){
	local duri
	duri=$(get_dhcp_var $interface bootfile)
	setmacro uci_uris "$duri"
	setmacro dhcp_uri "$duri"
}

dhcp_help(){
	echo "DHCP stage1 module"
}
