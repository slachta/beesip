#!/bin/sh

VERBOSITY=0
UCITMP=/tmp/uciprov
UCACHE=$UCITMP/cache
UFETCH=$UCITMP/fetch
CACHE_MAXAGE=180

blog(){
	echo "[uciprov] $@" >&2
}

mblog(){
	local module
	module=$1
	shift
	echo "[uciprov-$module] $@" >&2
}

blogn(){
	echo -n "[uciprov] $@" >&2
}

bloge(){
	echo "$@" >&2
}

dbg() {
    [ "$debug" -ge "$1" ] && shift && blog "$@"
    true
}

mdbg() {
    [ "$debug" -ge "$1" ] && shift && mblog "$@"
    true
}

uci(){
	/sbin/uci "$@" || echo uci "$@" >&2
}

far(){
   local f
   local t
	local sedcmd="sed"
	i=1
	while [ "$i" -lt "$#" ];
	do
		eval f=\$${i}
		i=$(expr $i + 1)
		eval t=\$${i}
		i=$(expr $i + 1)
		sedcmd="$sedcmd -e 's~$f~$t~g'"
	done
   eval $sedcmd
}

uciprov_hook_init() {                         
        local hook="${1}_hook"             
        export -n "UP_STACK_LIST=${UP_STACK_LIST:+$UP_STACK_LIST }$hook"
        export -n "$hook="                                              
}
                                                                    
uciprov_hook_add() {                                                    
        local hook="${1}_hook${UP_HOOK_SPLICE:+_splice}"                
        local func="${2}"
        shift; shift
                                                        
        [ -n "$func" ] && {
                local v; eval "v=\$$hook"
                export -n "$hook=${v:+$v }$func^$1^$2^$3^$4"
                dbg 3 "Adding hook $hook=${v:+$v }$func^$1^$2^$3^$4"
	}
}

uciprov_run_hook(){
   local hook="${1}_hook"
   local h
   shift
   
   local v;eval "v=\$$hook";
   for h in $v; do
      hargs=$(echo $h | tr '%' ' ')
      dbg 3 "Runing hook $hook ($hargs) $@"
      eval $(echo $hargs | (IFS="^"; read h a1 a2 a3 a4; echo \"$h\" \"$a1\" \"$a2\" \"$a3\" \"$a4\")) "$@"
   done;
}

uciprov_test_hook(){
  local hook="${1}_hook"
   
  local v;eval "v=\$$hook";
  [ -n "$v" ]
}

uciprov_show_hook(){
   local hook="${1}_hook"
   local h
   shift
   
   local v;eval "v=\$$hook";
   for h in $v; do
      hargs=$(echo $h | tr '%' ' ')
      echo $(echo $hargs | (IFS="^"; read h a1 a2 a3 a4; echo \"$h\" \"$a1\" \"$a2\" \"$a3\" \"$a4\")) $@
   done;
}


#
# function checking if overlay is succesfully mounted
#

check_overlay() {
   mount |grep /overlay | grep -q /dev/
}

#
# Function to check good network time
# 

check_nettime() {
   logread | grep -q " step time server "
}

#
# SSL rehash function
#

ssl_rehash() {
   if which openssl >/dev/null && [ -d /etc/ssl/certs ]; then
   (cd /etc/ssl/certs && \
      (
      find ./ -regex .*\.key -exec chmod 400 {} \;;
      [ -n "$1" ] && find ./ -type l -regex .*\.0$ -exec sh -c 'rm -f {}' \;;
      certs=$(find ./ -type l -exec readlink {} \;;)
      find ./ -name '*crt' -o -name '*pem' | \
       while read crt; do \
         echo $certs | grep -q $crt && continue
         hash=$(openssl x509 -noout -hash < "$crt");
         chmod 444 "$crt"; ln -s "$crt" "${hash}.0";
         blog "ssl_rehash: $crt => ${hash}.0"
       done
     )
   )
    fi
}

fetch_uri(){
	local muri
	local now
	local mtime
	mkdir -p $UCACHE
	muri=$(echo "$1" | md5sum | cut -d ' ' -f 1)
	now="$(date +%s)"
	# Check if cached result exists and is not older than CACHE_MAXAGE seconds
	if [ -f "$UCACHE/$muri" ]; then
	  mtime="$(date -r $UCACHE/$muri +%s)"
	  if [ $(expr $now - $mtime) -lt "$CACHE_MAXAGE" ]; then
	    dbg 3 "fetch_uri: cached $1 ($muri)"
	    cat "$UCACHE/$muri"; return;
	  else
	    rm -f "$UCACHE/$muri"
	  fi
	fi
	if [ -f "$UCACHE/$muri.err" ]; then
	  mtime="$(date -r $UCACHE/$muri.err +%s)"
	  if [ $(expr $now - $mtime) -lt "$CACHE_MAXAGE" ]; then
	    dbg 3 "fetch_uri: negative $1 ($muri)"
	    return 1
	  else
	    rm -f "$UCACHE/$muri.err"
	  fi
	fi
	echo "$1" >"$UCACHE/$muri.url"
	fetch_uri_real "$1" >"$UCACHE/$muri" || { mv -f "$UCACHE/$muri" "$UCACHE/$muri.err"; return 1; }
	[ $(wc -c <"$UCACHE/$muri") = 0 ] && { mv "$UCACHE/$muri" "$UCACHE/$muri.err"; return 1; }
case $1 in
*gz)
	gunzip -c "$UCACHE/$muri"
	;;
*)
	cat "$UCACHE/$muri"
	;;
esac
}

fetch_uri_real(){
	local tmpdir=$UFETCH/$$
	local uri="$1"
	local dst="$2"
	uciprov_test_hook uciprov_fetchuri "$1" "$2" && uciprov_run_hook uciprov_fetchuri "$1" "$2" && return
case $1 in
file:*|ftp:*|http:*|https:*|tftp:*)
	 curl --capath /etc/ssl/certs -f -s -L -o - "$1"
	;;
rsync:*)
	mkdir -p $tmpdir && \
	rsync "$uri" $tmpdir/ && \
	cat $tmpdir/$(basename "$1") && \
	rm -rf $tmpdir
	;;
ssh:*)
	local h=$(echo $uri | cut -d '/' -f 3)
	local hp=$(echo $uri | cut -d '/' -f 3-)
	local p=$(echo $uri | cut -d '/' -f 4-)
	ssh -y $h "cat $p 2>/dev/null || cat $p"
	;;
9p:*)
	local h=$(echo $uri | cut -d '/' -f 3)
	local p=$(echo $uri | cut -d '/' -f 4-)
	local f=$(basename $uri)
	mkdir -p $tmpdir && \
	mount -o trans=virtio,version=9p2000.L -t 9p "$h" $tmpdir && \
	cat $tmpdir/$(basename "$1") && \
	umount $tmpdir && \
	rm -rf $tmpdir
	;;
*)
	return 1
	;;
esac
}

test_uri(){
	local buf
	uciprov_test_hook uciprov_testuri "$1" && uciprov_run_hook uciprov_testuri "$1" && return
case $1 in
ftp:*|http:*|https:*|tftp:*|file:*)
	buf=$(curl --capath /etc/ssl/certs -m 5 -f -s -L -I "$1") && buf=$(echo -n "$buf" | dd bs=1 count=1 2>/dev/null| wc -c)
	[ "$buf" = "1" ]
	;;
*)
	md5=$(fetch_uri "$1" | md5sum | cut -d ' ' -f 1)
	[ "$md5" = "d41d8cd98f00b204e9800998ecf8427e" ] && return 1
	;;
esac
}

validate_uri(){
	[ -z "$1" ] && return 1
	test_uri "$1" || return 1
}

# push_uri "file" "uri" [content-type]
push_uri(){
local uri=$2
local file=$1
local ctype=$3
[ -n "$ctype" ] && ctype="-H 'Content-Type: $3'"
case $2 in
ftp:*|http:*|https:*|tftp:*)
    curl -s -f -L -X POST "$ctype" --data-binary @"$1" "$2"
    ;;
ssh:*)
    local h=$(echo $uri | cut -d '/' -f 3)
    local hp=$(echo $uri | cut -d '/' -f 3-)
    local p=$(echo $uri | cut -d '/' -f 4-)
    local b=$(basename $file)
    script="if [ -d '$p' ]; then cat >'$p/$b'; else cat >'$p' ; fi"
    cat $file | ssh -y $h "$script"
    ;;
esac
}

# $1 macro name
# $2 macro value
# $3 if nonempty, replace value instead of add to list
setmacro(){
	local mname=$1
	local mvalue=$2
	local val=$(getmacro $mname)
	if (echo $uci_macros|grep -qvw "$mname"); then
		export uci_macros="$uci_macros $mname"
		eval export ${mname}=\"$mvalue\"
	else
	  if [ -z "$3" ]; then
		eval export ${mname}=\"$val $mvalue\"
	  else
		eval export ${mname}=\"$mvalue\"
	  fi
	fi
}

getmacro(){
	local mname=$1
	eval local mvalue=\$${mname}
	echo $mvalue
}

listmacros(){
	for i in $uci_macros; 
	do
		echo $i
	done
}

getmacros(){
	for i in $uci_macros; 
	do
		echo "'{$i}' '$(getmacro $i)'"
	done
}

handle_macro(){
	local value=""
	local newvalue=""
	local type="static"
	local func
	
	config_get persist "$1" persist
	config_get value "$1" value
	config_get func "$1" func
	
	if [ "$type" = "dynamic" ] || [ -z "$value" ]; then
	  newvalue="$($func)"
	else
	  newvalue="$value"
	fi
	if  [ "$type" = "static" ] && [ -z "$value" ] && [ -n "$newvalue" ]; then
	  uci_set "uciprov" "$1" "value" "$newvalue"
	fi
	setmacro "$1" "$newvalue" replace
}

load_user_macros(){
	uci_load uciprov
	config_load uciprov
	config_foreach handle_macro macro
	uci_commit
}

prepare_uri(){
	local puri="$1"
	
	echo "$puri" | eval far $(getmacros)
}


# Prepare stage1 module. If binary is not executable, skip initialization. Return true or false
# $1 protocol
# $2 binary needed
uciprov_initmodule1(){
	local protocol="$1"
	local binary="$2"
	if type $1_help >/dev/null; then
	  uciprov_hook_add uciprov_help $1_help
	fi
	if echo " $protocols " | grep -qi " $protocol "; then
	  if [ -n "$binary" ]; then
	    if which "$binary" >/dev/null; then
	     mdbg 2 "S1:$protocol" "Enabled"
	      return
	    else
	      mdbg 2 "S1:$protocol" "Missing binary $binary. Disabling!"
	      return 1
	    fi
	  else
	    mdbg 2 "S1:$protocol" "Enabled"
	    return
	  fi
	else
	  mdbg 2 "S1:$protocol" "Disabled in config"
	  return 1
	fi
}

# Init stage2 module
# If $2 is given, force to init module even if prereqs are not met
# Returns uri on stdout or false
uciprov_initmodule2(){
	local module="$1"
	local muri
	local meuri
	local macroexpand
	local fdonly
	local factorydefault
	local enabled
	local run_on
	local binary="$2"
	local override="$3"

	config_load uciprov
	config_get_bool enabled $module enable 1
	config_get_bool fdonly $module only_on_fd 1
	config_get run_on $module run_on peruri
	config_get version $module version 0
	config_get_bool factorydefault global factorydefault
	config_get muri $module uri
	[ -z "$muri" ] && muri="$uri"
	
	if type $module_help >/dev/null; then
	  uciprov_hook_add uciprov_help $module_help
	fi

	if  [ -z "$override" ];
	then
	  if [ "$enabled" = 0 ] || ( [ -n "$binary" ] && !  which "$binary" >/dev/null ) || ( [ -n "$moduleonly" ] && [ "$module" != "$moduleonly" ] ) || ( [ "$factorydefault" = "0" ] && [ "$fdonly" = "1" ] )
	  then
		mdbg 1 "S2:$module" "Disabling (enable=$enabled, binary=$binary, fd=$factorydefault, fd_only=$fdonly, uri=$muri, cmdline=$moduleonly)"
		return 1
	  else
	        mdbg 1 "S2:$module" "Enabling (uri=$muri)"
	  fi
	fi
	
	case $run_on in
	
preinit)
	uciprov_hook_add uciprov_prestage2 uciprov_$module "$module" "$muri" "$version"
	;;
postinit)
	uciprov_hook_add uciprov_postinit uciprov_$module "$module" "$muri" "$version"
	;;
preapply)
	uciprov_hook_add uciprov_preapply uciprov_$module "$module" "$muri" "$version"
	;;
peruri)
	uciprov_hook_add uciprov_peruri uciprov_$module "$module" "$muri" "$version"
	;;
	esac
}

# Run module. Returns uri to fetch or false
# $1 - module
# $2 - uri
# $3 - version
# $4 - if set, ignore uri, module takes care itself
uciprov_runmodule2(){
	local md5
	local module="$1"
	local uri="$2"
	local uri2="$5"
	local version="$3"
	local meuri
	
	if [ -n "$uri2" ]; then
	  uri=$uri2;
	fi
	
	setmacro m "$module" replace
	meuri=$(prepare_uri "$uri")
	mdbg 3 $module "runmodule $@"
	
	if validate_uri $meuri;
	then
		md5=$(fetch_uri $meuri | md5sum - | cut -d ' ' -f 1)
		mdbg 3 $module "uri=$uri,euri=$meuri,version=$version,md5=$md5"
		if [ -n "$override" ]
		then
			echo $meuri
			return
		else
			if [ "$version" = "$md5" ]; then
		          mdbg 2 "$module" "Same version of $meuri content!"
		          return 1
		        else
		          if [ "$md5" = "d41d8cd98f00b204e9800998ecf8427e" ]; then
		            mdbg 2 "$module" "Empty url $meuri!"
		          else
			    echo $meuri
			    return
			  fi
			fi
		fi
	else
		mdbg 3 "$module" "Uri $meuri unreachable!"
		return 1
	fi
}

uciprov_donemodule2(){
	local module="$1"
	local version="$2"
	
	uci set uciprov.$module.version="$version"
	uci commit
}

restart_service(){
      [ -x /etc/init.d/$1 ] && blog "Restarting /etc/init.d/$1" && /etc/init.d/$1 restart
}

uci_apply(){
	local netrestart fwrestart sysrestart logrestart dhcprestart fstabrestart

	uciprov_run_hook uciprov_preapply
	uci batch
	uci changes | grep -qE '^(network|wireless)' && netrestart=1
	uci changes | grep -qE '^(firewall)' && fwrestart=1
	uci changes | grep -qE '^(system)' && sysrestart=1
	uci changes | grep -qE '^(system.*log_ip)' && logrestart=1
	uci changes | grep -qE '^(dhcp)' && dhcprestart=1
	uci changes | grep -qE '^(uciprov)' && uciprovrestart=1
	uci changes | grep -qE '^(fstab)' && fstabrestart=1
	dbg 3 "Applying UCI: $(uci changes |tr '\n' ' ')"
	uci commit
	uciprov_run_hook uciprov_postapply
	if [ -n "$uciprovrestart" ]; then
	  blog "Rereading uciprov config"
	  config_load uciprov
	  config_get interface "global" interface
	  export interface
	  config_get protocols "global" uri_retrieval_protocols
	  export protocols
	  config_get_bool factorydefault "global" factorydefault
	  export factorydefault
	  config_get_bool reboot "global" reboot_after_cfg_apply
	  config_get debug "global" debug
	fi
#	[ -n "$netrestart" ] && restart_service network && mblog "Sleeping 30 seconds to wakeup network" && sleep 30
#	[ -n "$fwrestart" ] && restart_service firewall 
#	[ -n "$sysrestart" ] && restart_service system 
#	[ -n "$logrestart" ] && restart_service log 
#	[ -n "$dhcprestart" ] && restart_service dnsmasq
#	[ -n "$fstabrestart" ] && restart_service fstab
	true
}

get_interface_mac_address(){
	#check if interface exists
	if [ -e /sys/class/net/$1/address ];
	then
		macAddr=$(cat /sys/class/net/$1/address)
		echo -ne $macAddr | sed 's/\:/-/g'
	else
		echo -ne ""
	fi
}

get_interface_ip(){
	#check if interface exists
	if [ -e /sys/class/net/$1 ];
	then
		ip=$(ifconfig $1| grep -o "inet addr\:[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*")
		echo -ne $ip
	else
		echo ""
	fi
}

get_net_ifaces(){
	ifaces=$(ls /sys/class/net/)
	echo -ne $ifaces
}

x86_board_name(){
        local hypervisor
        
	if cat /proc/cpuinfo | grep -q 'flags.*hypervisor'; then
	  if [ -f /proc/scsi/scsi ]; then
	   cat /proc/scsi/scsi  |grep -qi vmware && hypervisor=vmware
	   cat /proc/scsi/scsi  |grep -qi qemu && hypervisor=kvm
	  fi
	else
	  echo $(uname -m)-unknown
	  return
	fi
	echo $(uname -m)-$hypervisor
}

# generate random text length $1
gen_rand_txt(){
  (cat /dev/urandom | tr -dc "0-9a-zA-Z.,/\:\-\+\#\%\*" |head -c "$1") 2>/dev/null
}

# generate random hash
gen_rand_hash(){
  gen_rand_txt 200 | md5sum - | cut -d ' ' -f 1
}

uciprov_fd(){
  export CONF_TAR=$UCITMP/su.tgz
  for s in /lib/upgrade/*sh; do
  . $s
  done
  if [ -z "$1" ]; then
    uci set uciprov.global.factorydefault=1
    uci commit
    tar -czf $UCITMP/su.tgz /etc/config/uciprov /etc/dropbear/
    copy=jffs2_copy_config
  else
    copy=true
  fi
  kill_remaining KILL
  if mount | grep -q jffs2; then
    run_ramfs ". /lib/functions.sh; include /lib/upgrade; $copy; reboot -f"
  fi
  dev=$(mount | grep ^/ | grep /overlay | cut -f 1 -d ' ' )
  [ -n "$dev" ] && which beesip >/dev/null && beesip fd "$dev"
}
