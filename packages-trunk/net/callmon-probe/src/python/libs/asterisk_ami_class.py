#!/usr/bin/env python
# -*- coding: utf-8 -*-

import telnetlib as tl
import re

AMI_HOST     = '127.0.0.1'
AMI_PORT     = 5038

class AstConnection(object):
    """
    Class to make connection to Asterisk AMI interface.
    """
    def __init__(self, cfg):
        self.cfg    = cfg
        self.conn   = None
        self.host   = AMI_HOST
        self.port   = AMI_PORT
        self.user   = self.cfg.asterisk_ami_user
        self.secret = self.cfg.asterisk_ami_secret

    def connect(self):
        try:
            if not self.conn:
                self.conn = tl.Telnet(
                                        self.host,
                                        self.port
                                     )

        except:
            raise Exception("Connection to Asterisk AMI failed.")
        return

    def send_action(self, action='login', attributes=None):
        """Action has to be string, attributes dictionary"""
        attributes = attributes or dict(Username=self.user, Secret=self.secret, Events='off') # default action should be login, thus using local instance variables
        if self.conn:
            self.conn.write("Action: %s\r\n" % action)
            if attributes and isinstance(attributes, dict):
                for key, value in attributes.iteritems():
                    self.conn.write("%s: %s\r\n" % (key, value))
            self.conn.write("\r\n")


    def read_response(self, check_for=None):
        response = self.conn.read_until("\r\n\r\n")
        retval = False
        if check_for:
            retval = True if re.search(check_for, response) else False
        return retval


