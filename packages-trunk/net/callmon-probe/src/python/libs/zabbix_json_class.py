#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from pprint import pprint
import json


# Rozhrani mezi sondou a serverem využívající JSON RPC rozhraní zabbixu. Využívá autentikaci, tanuje umo�nuje prováet té všechny nastavení zabbix serveru
class ZabbixJson(object):
    def __init__(self, cfg):
        self.Cfg            = cfg
        self.url            = self.Cfg.zabbix_url + '/api_jsonrpc.php'        # Url rozhrani pro komunikaci
        self.Session        = None                                      # Promena pro ulozeni sessiony po uspesnem provedeni autentikace
        self.Headers        = {'content-type': 'application/json',}     # Hlavicky pro zasilani pozadavku
        self.Active_items   = []                                   # Pole pro ulozeni aktivnich itemu ziskanych ze serveru
        self.auth()                 # Pri iniciaci trídy se automaticky provád�rihlášení a uložení session id
        #if(self.auth() == True):
            #self.get_host()
    # obalení dat do json struktury, ridání session id a zaslání na server
    def request(self, method, params):
        payload = {
            "jsonrpc" : "2.0",
            "method" : method,
            "params": params,
            "auth" : self.Session,
            "id" : 2,
        }
        result  = requests.post(self.url, data=json.dumps(payload), headers=self.Headers)
        result = result.json();
        return  result

    # zaslánírihlášovacích údajů a uložení session id
    def auth(self):
        result = self.request("user.login",{
                    'user': self.Cfg.zabbix_login,
                    'password': self.Cfg.zabbix_password,
                 })
        if hasattr(result, 'error') == False:
            self.Session = result['result']
            #print "Connected to zabbix server"
            return True
        else:
            print "ERROR ",result['error']['code']," - ",result['error']['data'];
            return False

    #nastaveni stavu sondy na aktivni
    def activate_host(self, ip):
        result = self.request("host.get",{'output': ['hostid'],'filter':{'name':ip} })
        try:
            id = result['result'][0]['hostid']
        except IndexError:
            id = 0
        if id != 0:
            res2 = self.request("host.update",{'hostid':id ,'status':0})
            return True
        else:
            return False

    #získání seznamu sond
    def get_hosts(self, atributes):
        result = self.request("host.get",{'output': atributes,'filter':{'status':[0]} }) #'extend'
        return result['result']

    def get_host_id(self, host):
        result = self.request("host.get",{
            "output":['id'],
            "filter":{'host':[host]}
        })
        try:
            id = result['result'][0]['hostid']
        except IndexError:
            id = 0
        return id

    #kontrola zda sonda s timto nazvem uz existuje
    def check_host(self, name):
        result = self.request("host.exists",{"host": name})
        if result['result'] == True:
            return True
        else:
            return False

    #přidání sondy do seznamu na serveru params: viz controller funkce create_probe
    def add_host(self, params):
        result = self.request("host.create", params)
        return result

    #ziskani historie daneho itemu a dane sondy... nedodelane
    def get_history(self):
        result = self.request("history.get",  {
            "history": 4,
            "itemids": [self.Active_items[0]['itemid']],
            "output":"extend"})

    def add_item(self, name, host):
        host_id = self.get_host_id(host)
        if host_id != 0:
            result = self.request("item.create",{
                "name": name,
                "key_": name,
                "hostid": host_id,
                "type": 7,
                "value_type": 4, #pripadne lze zvolit typ 0 jako float, ale zaokrouhluje na 2 desetina mista
                "interfaceid": "11",
                "applications": [],
                "delay": 30,
                "status": 0
            })
        else:
            print "Host not found"


    def check_item(self, name, host):
        result = self.request("item.exists",{
             "host": host,
             "key_": name
        })
        try:
            ret = result['result']
        except AttributeError:
            ret = False
        return ret


