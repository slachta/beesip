#!/usr/bin/env python
# -*- coding: utf-8 -*-
import paramiko
import traceback
import sys

#Trida pro pripojeni ke vzdalenemu pocitaci pomoci SSH
class Ssh(object):
    ssh = object    #objekt udrzujici handler daneho pripojeni

    def __init__(self, cfg, ttype):
        self.Cfg = cfg
        self.login(self.Cfg.host, ttype)          # pri iniciaci provede prihlaseni k danepu stroji
    # stazeni souboru pomoci sftp
    def get_file(self, f, target_location, save_location):
        sftp = paramiko.SFTPClient.from_transport(self.ssh)
        sftp.get(target_location + f, save_location + f)
    # stazeni textoveho souboru pomoci ssh + cat
    def download(self, f, target_location, save_location):
        data = self.command('cat ' + target_location + f)
        if data:
            for line in data.readlines():
                output = open(save_location + f,'wb')
                output.write(line)
                output.close()
    #spusteni libovolneho ssh prikazu na vzdalenem stroji
    def command(self, command):
        stdin, stdout, stderr  = self.ssh.exec_command(command)
        err = stderr.read();
        if  err:
            print err
            return False
        return stdout
    # prihlaseni ke vzdalenemu stroji
    def login(self, host, ttype):
        try:
            if ttype == 'ftp':
                self.ssh = paramiko.Transport((host, 22))
                self.ssh.connect(
                        username = Cfg.server_ssh_login_username,
                        key_filename=Cfg.rsa_key_path
                        )
            elif ttype == 'ssh':
                self.ssh = paramiko.SSHClient()
                self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                self.ssh.connect(
                        host,
                        username = Cfg.server_ssh_login_username,
                        key_filename = Cfg.rsa_key_path
                        )
        except Exception:
            print traceback.print_exc()
            sys.exit()

