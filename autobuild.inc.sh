
if ! [ -f ./inc/config.mk ]; then
  echo "Not in BEESIP build directory!"
  exit 1
fi

printSection () {
	echo "*******************************************"
	echo -e "\t"$1
	echo "*******************************************"
}

gettargets () {
  for i in $(ls targets/*mk); do
    echo $i | cut -d '/' -f 2 | cut -d '-' -f 1-10 | cut -d '.' -f -1
  done
}

gettargetsdbg () {
  for i in $(ls targets/*mk); do
    echo $i | cut -d '/' -f 2 | cut -d '-' -f 1-4 | cut -d '.' -f -1 | tr -d '\n'
    echo -dbg
  done
}

targethw () {
  echo $1 | cut -d '-' -f 1 | cut -d '.' -f -1
}

targetarch () {
  echo $1 | cut -d '-' -f 2 | cut -d '.' -f -1
}

targetowrt () {
  local t=$(echo $1 | cut -d '-' -f 3 | cut -d '.' -f -1)
  case $t in
bb)
	echo barrier_breaker
	;;
cc)	echo chaos_calmer
	;;
dd)	echo designated_driver
	;;
*)	echo $t
	;;
  esac
}

dbg() {
  if [ -n "$TST" ]; then
    echo $*
  else
    echo $* ; $*
  fi
}

dotarget() {
  unset PATH
  unset BCPU
  unset BHW
  unset OWRT
  unset CONF
  unset CLEAN
  unset DIRCLEAN

  export PATH=$OLDPATH
  if ! [ -f $LOCKFILE ] || [ -n "$TST" ] ; then
    if ! [ -d "$OWRT_DIR" ]; then
	echo "You must do update for target first! $OWRT_DIR does not exist yet (PATH=$PATH)."
    else
      if [ -n "$TST" ]; then
  	unset TST
        make info env-info CFG=$TARGETCFG $MSEPARATE $MKVERBOSE $MDBG;
        #make -n beesip-images CFG=$TARGETCFG $MSEPARATE $MKVERBOSE $MDBG
        exit;
      fi
    fi
    dbg touch $LOCKFILE
    dbg make -s $* CFG=$TARGETCFG $MSEPARATE $MKVERBOSE $MDBG; ret=$?
    dbg rm -f $LOCKFILE
    return $ret
  else
     echo "$LOCKFILE exists! Exiting."
     false
  fi
}

preparetarget() {
  dotarget prepare
}

cleantarget() {
  dotarget clean
}

dircleantarget() {
  dotarget dirclean
}

buildtarget() {
  if [ -z "$FBUILDONLY" ]; then
    dotarget build
  else
    dotarget fastimg
  fi
}

configtarget() {
  dotarget config
}


shelltarget() {
  dotarget test-shell
}


cleanrepo() {
  rm -rf $REPODIR
  exit
}

_mkdir() {
	mkdir -p $*
}

_cp() {
	[ -f "$1" ] || [ -d "$1" ] && rsync -r $*
}

mkrepo() {
  if ! which ipkg-make-index.sh >/dev/null; then
    echo "You have to build basic target first, do $0 update,prepare,build (OWRT_DIR=$OWRT_DIR,PATH=$PATH)"
    exit 3
  fi
  TOP=$PWD
  for i in $(gettargets); do
    hw=$(targethw $i)
    arch=$(targetarch $i)
    owrt=$(targetowrt $i)
    REPOROOT=$VREPODIR/$BEESIP_VERSION
    REPOD=$REPOROOT/owrt_$owrt/$arch
    REPOD2=$REPOROOT/owrt_$owrt/$arch-dbg

    _mkdir $REPOROOT $REPOROOT/util/
    _cp $PWD/scripts/mkrepo.sh $REPOROOT/util/
    _cp $OWRT_DIR/scripts/ipkg-make-index.sh $REPOROOT/util/
    _cp $OWRT_DIR/scripts/ipkg-build $REPOROOT/util/
    _cp $OWRT_HOST_DIR/ipkg-buildpackage $REPOROOT/util/
    _cp $OWRT_HOST_DIR/opkg $REPOROOT/util/
    if [ -d build/owrt-$arch-$owrt/bin/$arch/packages ] ; then
      echo -e "\nCopying packages build/owrt-$arch-$owrt/bin/$arch/packages to $REPOD...\n"
      _mkdir $REPOD/packages
      _cp build/owrt-$arch-$owrt/bin/$arch/packages/ $REPOD/packages/
    else
      echo "Warning! No packages for $hw-$arch-$owrt (build/owrt-$arch-$owrt/bin/$arch/packages) !"
    fi
    if [ -d build/owrt-$arch-$owrt-dbg/bin/$arch/packages ]; then
      _mkdir $REPOD2
      echo -e "Copying packages build/owrt-$arch-$owrt-dbg/bin/$arch-dbg/packages to $REPOD2...\n"
      _mkdir $REPOD2/packages
      _cp build/owrt-$arch-$owrt-dbg/bin/$arch/packages/ $REPOD2/packages/
    fi
  done
  (cd $REPOROOT; ./util/mkrepo.sh)
  exit
}

waitfortmuxjobs() {
  if [ $($TMUX list-windows | wc -l) -gt "$1" ]; then
    echo "Maximum jobs reached ($1). Increase JOBS variable in autobuild.local.sh. Waiting."
  else
    if $TMUX list-windows | grep -qe "$2"; then
      echo "Cannot paralel build for same architecture and openwrt ($2). Waiting."
    else
      return
    fi
  fi
  while [ $($TMUX list-windows | wc -l) -gt "$1" ] || $($TMUX list-windows | grep -qe "$2"); do
    sleep 10
    $TMUX rename-window "wait"
    echo -n "."
  done
  echo
}

_yes() {
  i=1;
  while [ "$i" -lt 50 ]; do
    echo "y"
    i=$(expr $i + 1)
  done
}

REPODIR=$PWD/build/repo
VREPODIR=$REPODIR/$BEESIP_VERSION
if [ -d $PWD/build/owrt-x86-trunk ]; then
  OWRT_DIR=$PWD/build/owrt-x86-trunk
else
  OWRT_DIR=$PWD/build/owrt-x86-chaos_calmer
fi
OWRT_HOST_DIR=$OWRT_DIR/staging_dir/host/bin
CP="cp -u --reflink=auto"
CPR="$CP -u -R"
OLDPATH=$PATH
export PATH=$OWRT_HOST_DIR:$OWRT_DIR/scripts:$PATH

if which tmux >/dev/null 2>&1; then
  export TMUX="$(which tmux)"
else
  export TMUX="echo"
fi
