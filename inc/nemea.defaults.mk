# Defaults for eduroom images

BEESIP_PACKAGES += nemea-framework=y nemea-flow_meter=y curl=y uciprov=y

OWRT_CONFIG_UNSET += UCIPROV_AUTOSTART

$(eval $(call AddDefaultOpenWrtFeed,src-git nemea https://github.com/CESNET/Nemea-OpenWRT))

