# Defaults for x86 targets. Can be included directly from target.
# Load defaults for all x86 (32bit and 64bit)

$(eval $(call BeesipDefaults,x86))

TARGET_CPU=x86
TARGET_QEMU=i386
TARGET_QEMU_OPTS=-m 512
OWRT_CONFIG_SET+=TARGET_x86=y TARGET_BOARD=\"x86\" HAS_SUBTARGETS=y
