
ifeq ($(OWRT_GIT),)
OWRT_GIT="git://git.openwrt.org/15.05/openwrt.git"
endif

USE_CUSTOM_FEEDS=1

$(eval $(call AddDefaultOpenWrtFeed,src-git packages https://github.com/openwrt/packages.git;for-15.05))
$(eval $(call AddDefaultOpenWrtFeed,src-git luci https://github.com/openwrt/luci.git;for-15.05))
$(eval $(call AddDefaultOpenWrtFeed,src-git routing https://github.com/openwrt-routing/packages.git;for-15.05))
$(eval $(call AddDefaultOpenWrtFeed,src-git telephony https://github.com/openwrt/telephony.git;for-15.05))
$(eval $(call AddDefaultOpenWrtFeed,src-git management https://github.com/openwrt-management/packages.git;for-15.05))
$(eval $(call AddDefaultOpenWrtFeed,src-git zabbix https://github.com/limosek/zabbix-owrt.git))
