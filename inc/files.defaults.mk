# Files which are aded during compilation
OWRT_ADDED_FILES += \
	pbx-files/etc/lighttpd/lighttpd.conf:/etc/lighttpd/lighttpd.conf \
	pbx-files/etc/lighttpd/lighttpd.user:/etc/lighttpd/lighttpd.user \
	pbx-files/etc/profile:/etc/profile \
	pbx-files/etc/banner:/etc/banner \
	pbx-files/etc/config/system:/etc/config/system \
	pbx-files/uci-defaults/passwd-prep:/etc/uci-defaults/passwd-prep \
	pbx-files/etc/kamailio/kamailio.cfg:/etc/kamailio/kamailio.cfg \
	pbx-files/etc/kamailio/tls.cfg:/etc/kamailio/tls.cfg \
	pbx-files/etc/kamailio/defines.cfg:/etc/kamailio/defines.cfg \
	pbx-files/etc/kamailio/vars.cfg:/etc/kamailio/vars.cfg \
	pbx-files/etc/kamailio/modules2.cfg:/etc/kamailio/modules2.cfg \
	pbx-files/etc/kamailio/ldap.ini:/etc/kamailio/ldap.ini \
	pbx-files/etc/kamailio/kamctlrc:/etc/kamailio/kamctlrc

# Files which are added during first boot from defaults
OWRT_ADDED_ETCDEFAULT += etc/config etc/beesip etc/ssl etc/asterisk etc/lighttpd etc/snort etc/ssh www
