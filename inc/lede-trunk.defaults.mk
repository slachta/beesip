
OWRT_GIT="https://git.lede-project.org/source.git"

USE_CUSTOM_FEEDS=1

$(eval $(call AddDefaultOpenWrtFeed,src-git packages https://git.lede-project.org/feed/packages.git))
$(eval $(call AddDefaultOpenWrtFeed,src-git luci https://github.com/openwrt/luci.git))
$(eval $(call AddDefaultOpenWrtFeed,src-git routing https://git.lede-project.org/feed/routing.git))
$(eval $(call AddDefaultOpenWrtFeed,src-git telephony https://git.lede-project.org/feed/telephony.git))
$(eval $(call AddDefaultOpenWrtFeed,src-git management https://github.com/openwrt-management/packages.git))

