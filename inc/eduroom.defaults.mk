# Defaults for eduroom images

BEESIP_PACKAGES += \
	eapwatchdog=y softflowd=y kmod-pppoe=y ppp=y ppp-mod-pppoe=y \
	addrwatch=y \
	lldpd=n tcpdump=y \
	tftp-hpa=n ip=y \
	wpad=y hostapd=n \
	openvpn-openssl=y openssl-util=y \
	iftop=y uciprov=y ca-certificates=y \
	zabbix3-agentd=y

OWRT_CONFIG_SET += \
	UCIPROV_USE_STATIC=y \
	UCIPROV_URI1="https://eduroom.cesnet.cz/uci/default/{cn}/{m}.{fde}" \
	UCIPROV_URI2="https://eduroom.cesnet.cz/uci/{board}/{cn}/{m}.{fde}" \
	UCIPROV_URI3="https://eduroom.cesnet.cz/uci/{cn}/{mac}/{m}.{fde}" \
	UCIPROV_INTERFACE="wan" \
	UCIPROV_TGZ=y \
	UCIPROV_SU=y \
	UCIPROV_RECOVERY=y \
	UCIPROV_OPKG=y \
	UCIPROV_SERVICES=y \
	UCIPROV_SSLDEPLOY=y \
	UCIPROV_TGZ_ONLYFD=y \
	UCIPROV_SU_ONLYFD=y \
	UCIPROV_DELAY_BEFORE_REBOOT="180" \
	UCIPROV_AUTOSTART=y \
	UCIPROV_REPEAT="120" \
	UCIPROV_RECOVERY_FDFLAG="EduRoom" \
	UCIPROV_MGMT=y \
	UCIPROV_MGMT_SETROOTPW=y \
	UCIPROV_REBOOT_AFTER_CFG_APPLY=y \
	UCIPROV_SSLDEPLOY_FETCHURI="https://eduroom.cesnet.cz/uci/ssldeploy/download.php?mac:{mac}" \
	UCIPROV_SSLDEPLOY_PUSHURI="https://eduroom.cesnet.cz/uci/ssldeploy/upload.php?mac:{mac}" \
	UCIPROV_SSLDEPLOY_SUBJECT="{mac}"


