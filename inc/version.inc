
# Version specific settings
# This file should be includable either via Makefile or via shell!

BEESIP_VERSION=1.4.1
KAMPKG=kamailio4
ASTPKG=asterisk11
OWRT_FORCE_GIT_REV=master
OWRT_FORCE_PACKAGES_GIT_REV=master
OWRT_FORCE_TELEPHONY_GIT_REV=master
OWRT_FORCE_ROUTING_GIT_REV=master
OWRT_FORCE_LUCI_GIT_REV=master

