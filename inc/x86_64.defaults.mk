# Defaults for x86_64 targets. Can be included directly from target.
# Load defaults for all x86 (32bit and 64bit)

$(eval $(call BeesipDefaults,x86))

TARGET_CPU=x86
TARGET_QEMU=x86_64
TARGET_QEMU_OPTS=-m 512
TARGET_SUFFIX=_64
OWRT_CONFIG_SET += TARGET_x86=y TARGET_x86_64=y HAS_SUBTARGETS=y TARGET_BOARD="x86" TARGET_ARCH_PACKAGES="x86_64"

# Disk image (can be directly booted or flashed)
OWRT_IMG_DISK_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-64-combined-squashfs.img

# Kernel image
OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-64-vmlinuz

# Squashfs rootfs
OWRT_IMG_SQUASHFS_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-64-rootfs-squashfs.img

# Sysupgrade is disc
OWRT_IMG_SYSUPGRADE_NAME=$(OWRT_IMG_DISK_NAME)

