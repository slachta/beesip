
/*
 Disable including system includes and libraries from cross-compiling
 Redirect binaries to right (cross) location
 if CH_DEBUG env is set, be more verbose
 if CH_PNAMES - names of binaries which should redirect, rest will be untouched. Can use wildcards. Eg. '*gcc:*ld'
 CH_RDIRS - names of directories to redirect to toolchain ('/usr:/home/toolchain;/lib:/home/toolchain')

example:

$ export LD_PRELOAD=$PWD/crosshlp.so.1 
$ export CH_DEBUG=1
$ export CH_RDIRS='/usr/include:/home/aaa/toolchain/;/usr/lib:/home/aaa/toolchain/'
$ export CH_PNAMES='*gcc:*ld'
$ make ...

*/
 	
#include <fcntl.h>
#include <sys/stat.h> /* for mode definitions */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libgen.h>
#include <stdarg.h>
#include <unistd.h>

#define __USE_GNU
#include <dlfcn.h>

#define MAXSTRLEN 2048

char *getprocname() {
	FILE *cf;
	char *processname=malloc(MAXSTRLEN);
	
	cf=fopen("/proc/self/cmdline","r");
	fgets(processname,128,cf);
	fclose(cf);
	return(basename(processname));
}

int wildcmp(const char *wild, const char *string) {
  // Written by Jack Handy - <A href="mailto:jakkhandy@hotmail.com">jakkhandy@hotmail.com</A>
  const char *cp = NULL, *mp = NULL;

  while ((*string) && (*wild != '*')) {
    if ((*wild != *string) && (*wild != '?')) {
      return 0;
    }
    wild++;
    string++;
  }

  while (*string) {
    if (*wild == '*') {
      if (!*++wild) {
        return 1;
      }
      mp = wild;
      cp = string+1;
    } else if ((*wild == *string) || (*wild == '?')) {
      wild++;
      string++;
    } else {
      wild = mp;
      string = cp++;
    }
  }

  while (*wild == '*') {
    wild++;
  }
  return !*wild;
}

int min(a,b) {
	if (a<b) return(a); else return(b);
}

open (__const char *__file, int __oflag, ...)  {
	va_list arguments;
	char *dn=malloc(strlen(__file));
	char *file=malloc(MAXSTRLEN);
	char *tmp,*tmp2;
	char *processname=getprocname();
	char *pnames=malloc(MAXSTRLEN);
	char *rdirs=malloc(MAXSTRLEN);
	char *rdir;
	char *saveptr;
	char *pname;
	int debug=(getenv("CH_DEBUG") != NULL);
	int redir=0;
	int flags=0;
	int flag;

	strcpy(pnames,getenv("CH_PNAMES"));
	strcpy(rdirs,getenv("CH_RDIRS"));
	strcpy(dn,__file);
	dn=dirname(dn);

	va_start ( arguments, __oflag );           
    	for (;;)        
    	{
		flag=va_arg(arguments, int ); 
		if (flag) {
        		flags+=flag;
		} else {
			break;
		}
    	}
    	va_end (arguments);

	pname=strtok_r(pnames,":",&saveptr);
	if (pnames && !pname) pname=pnames;
	while (pname) {
		if (wildcmp(pname,processname)) {
			redir=1; break;
		}
		pname=strtok_r(NULL,":",&saveptr);
	}

	if (!redir) return(__open(__file,__oflag,flags));

	if (debug) {
		 fprintf(stderr,"%s(%s): ",processname,pname);	
	}
	
	rdir=strtok_r(rdirs,";",&saveptr);
	if (rdirs && !rdir) rdir=rdirs;
	while (rdir) {
		tmp=strchr(rdir,':');
		tmp2=rdir+(tmp-rdir)+1;
		if (tmp) {
			//fprintf(stderr,"%s(%s)?%s %s ",__file,rdir,tmp+1,dn);	
			if (!strncmp(rdir,dn,min(tmp-rdir,strlen(dn)))) {
				sprintf(file,"%s%s",tmp2,__file);
				if (debug) { 
					fprintf(stderr," %s>%s\n",__file,file);	
				}
				return(__open(file,__oflag,flags));
			}
		}
		rdir=strtok_r(NULL,";",&saveptr);
	}

	return(__open(__file,__oflag,flags));
}

/*
int execve(const char *filename, char *const argv[], char *const envp[]) {
	char *pname,*saveptr;
	char *rpnames=getenv("CH_RPNAMES");
	int redir=0;
	char *file=malloc(MAXSTRLEN);
	char *tmp,*tmp2;
	int debug=0;
	static int (*__execve)(const char *, char * const*, char * const*)=NULL;

  	if (!__execve) __execve=dlsym(RTLD_NEXT,"execve");

	exit;
	pname=strtok_r(rpnames,":",&saveptr);
	if (rpnames && !pname) pname=rpnames;
	while (pname) {
		tmp=strchr(pname,':');
		tmp2=pname+(tmp-pname)+1;
		if (tmp) {	
			fprintf(stderr,"%s(%s)?%s ",filename,pname,tmp+1);	
			if (!strncmp(pname,filename,tmp-pname)) {
				sprintf(file,"%s%s",tmp2,filename);
				if (debug) { 
					fprintf(stderr," %s>%s\n",filename,file);	
				}
				return(__execve(file,argv,envp));
			}
		}
		pname=strtok_r(NULL,":",&saveptr);
	}

	if (!redir) return(__execve(filename,argv,envp));
}
*/


