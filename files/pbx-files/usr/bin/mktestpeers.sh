#!/bin/sh

engine="$1"
start="$2"
end="$3"
pw="$4"

if [ -z "$engine" ] || [ -z "$start" ] || [ -z "$end" ]; then
  echo "$0 engine start end [pw] [options]"
  echo "This script will generate peers on stdout. If used for asterisk, it is sip.conf format. For kamailio, it generates kamctl commands."
  echo "To generate kamailio peers 1000-2000 with common password 'password:'"
  echo "$0 kamailio 1000 2000 password"
  echo "To generate kamailio peers 1000-2000 with password same to peer name:"
  echo "$0 kamailio 1000 2000"
  echo "To generate asterisk peers"
  echo "$0 kamailio 1000 2000 '' 'nat=yes'"
  echo "Similar with kamailio"
  echo
  exit 2
fi

asterisk_peer(){
  local secret="$1"
  [ -n "$2" ] && secret="$2"
  cat <<EOF
[$1]
type=friend
callerid="mktestpeer $1" <$1>
username=$1
host=dynamic
secret=$secret
dtmfmode=rfc2833
insecure=invite,port
canreinvite=yes
nat=nat=force_rport,comedia
qualify=yes
mailbox=$1@default
$3
EOF
}

kamailio_peer(){
  local secret="$1"
  [ -n "$2" ] && secret="$2"
  echo kamctl rm \"$1\"
  echo kamctl add \"$1\" \"$secret\"
}

peer=$start;
while [ $peer -lt $end ]; do
  if [ "$engine"  = "asterisk" ]; then
    asterisk_peer $peer $pw
  fi
  if [ "$engine"  = "kamailio" ]; then
    kamailio_peer $peer $pw
  fi
  peer=$(expr $peer + 1)
done
