<?php

# *** PHP script for initiating computing quality ***

$file = fopen("_status.log", "r");
$status = fread($file, 1);
fclose($file);

if ($status == 1) {
  $file = fopen("_status.log","w");
  fwrite($file,"0");
  fclose($file);
  shell_exec("sh _stop.sh");

  shell_exec("sh _perform.sh");

  $file = fopen("_status.log","w");
  fwrite($file,"1");
  fclose($file);
  $PID = "_streams_pid.txt";
  $cmd = "sh _start.sh";
  shell_exec("nohup $cmd > /dev/null 2> /dev/null & echo $! > $PID");
} else {
  shell_exec("sh _perform.sh");
}
  
?>