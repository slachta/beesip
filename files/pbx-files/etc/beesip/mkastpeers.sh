#!/bin/sh

if [ -z "$1" ]; then
  echo "Enter number of peers ($0 numberofpeers [type])"
  exit 2
fi
numpeers=$1

i=1;
while [ $i -lt $numpeers ]; do
  if [ -z "$2" ]; then
    testpeer1 $i
  else
    testpeer2 $i
  fi
  i=$(expr $i + 1)
done

testpeer1 () {
  cat <<EOF
[$1]
type=friend
callerid="WOW $1" <$1>
username=$1
host=dynamic
secret=$1
dtmfmode=rfc2833
insecure=invite,port
canreinvite=yes
nat=yes
qualify=yes
mailbox=$1@default

EOF
}

testpeer2 () {
  cat <<EOF
[$1]
type=friend
host=10.0.0.1
disallow=all
allow=alaw
qualify=no
nat=no
secret=test

EOF
}

