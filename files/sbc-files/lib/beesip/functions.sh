
. /lib/uciprov/functions.sh

#
# logging function
#

blog() {
        logger -s -t "[Beesip]" $*
}

# Beesip environment get

beesip_env() {
        local section="directories"
        config_get BEESIP_CONFDBDIR "${section}" confdbdir /var/beesip
        config_get BEESIP_VARBDIR "${section}" vardbdir /var/beesip
        config_get BEESIP_STATDBDIR "${section}" confdbdir /var/beesip
        config_get BEESIP_STAMPDIR "${section}" confdbdir /var/lib/beesip
}

printMacAddress(){
    macAddr=$(cat /sys/class/net/$1/address)
    echo -ne $macAddr
}

printIP(){
    ip=$(ifconfig $1| grep -o "inet addr:[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"| grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*")
    echo -ne $ip
}

printNetInterfaces(){
    ifaces=$(ls /sys/class/net/)
    echo -ne $ifaces
}

printRootfsUsage(){
    echo -ne $(df -h |grep rootfs|grep -o "[0-9]*%")
}

printFreeMemory(){
    echo -ne $(free | awk '/^Mem:/{print $3}')
}

printAvailableMemory(){
    echo -ne $(free | awk '/^Mem:/{print $2}')
}

printProcessorLoad(){
    echo -ne $(uptime |awk -F'average:' '{ print $2}')
}

sysinfo(){
    echo -e "  System information as of "$(date)"\n"
    ifaces=$(printNetInterfaces)

    for i in $ifaces
    do
            echo -e "  Interface "$i":"
            echo -e "\t\t\t"$(printIP $i)
            echo -e "\t\t\t"$(printMacAddress $i)
    done

    echo -e "  Rootfs usage:\t\t"$(printRootfsUsage)
    echo -e "  Memory usage:\t\t"$(printFreeMemory) "KBytes used of" $(printAvailableMemory) "KBytes available"
    echo -e "  Processor load: \t"$(printProcessorLoad)"\n"

    if ! check_overlay;
    then
	echo -e "  Overlay not mounted!"
    fi

}

# function beesip_help
#
# returns basic parameters for a beesip command

beesip_help() {
cat <<EOF

  Beesip commandline utility.

  Usage:
    beesip help
	- This command prints help for beesip command

    beesip {factory-defaults,fd}
	- This command on squashfs filesystems restores system
	  to initial state.

    beesip patch
	- This command updates beesip to latest patchlevel for this release.

    beesip {crash-reporting,cr} {start,stop,collect}
	- This command controls crash-reporting system. If collect
	  parameter is called, it creates an archive of overlay,
	  collected logs and overlay to be used for debugging in
	  /root/ directory.

    beesip {system-upgrade,su} {path to Beesip firmware image}
	- This command upgrades system with provided firmware image.

    beesip import-gateways {stdout}
	- This command imports VoIP gateways into specified engine
	  in /etc/config/beesip. Optionally you can run it with stdout
	  parameter which only prints requested data to stdout.

    beesip add-local-gateway address [description]
    beesip del-local-gateway address
    beesip add-local-prefix prefix address [description]
    beesip del-prefix prefix
    beesip show-gateways
    beesip show-prefixes

    beesip sysinfo
	- This command prints on stdout system information as free
	  memory, cpu usage or disk usage.

EOF
}

beesip_download_if_not_empty(){
	echo "Creating $(basename $1)"
	data=$(wget --no-check-certificate -q -O - $2)
	echo "Downloading from "$2

	if [ "$data" != "" ]; then
		wget --no-check-certificate -q --output-document=$1 $2
		echo -e "OK\n"
	else
		echo -e "Obtained empty data."
		echo -e "Nothing to do."
	fi
}

beesip_import_prefixes_drouting(){
	(echo "BEGIN TRANSACTION;"
	IFS=';'
	cat /etc/kamailio/prefixes.txt | while read prefix length;
	do
	  echo "delete from dr_rules where groupid='1' and prefix='$prefix';"
	  echo 'insert into dr_rules ("groupid", "prefix", "timerec", "priority", "routeid", "gwlist", "description") values '" ('1', '$prefix', '20000101T083000', '0', '0', '1', '');"
	done
	echo "COMMIT;") | sqlite3 /etc/kamailio/kamailio.db
	kamcmd -s unixs:/var/run/kamailio_ctl drouting.reload
}

beesip_import_gateways(){
	config_load "beesip"

	config_get engine "gateways" import_for_engine
	config_get uri "gateways" import_uri

	if [ "$engine" == "" ]; then
		# default cfg engine
		engine="ast11"
	fi

	if [ "$uri" == "" ]; then
		# if uri is not set, then set default uri to gateway database export
		uri=https://iptelix.cesnet.cz/SIP-routing
	fi

	beesip_download_if_not_empty /etc/kamailio/prefixes.txt $uri"/../prefixExport"
	beesip_import_prefixes_drouting
}

beesip_factorydefaults() {
        local cmddev=$1
        local dev
        
	if kernel_getflag overlay;
	then
		ovr=$(kernel_getvar overlay)
		if echo $ovr | grep -q ',' && [ -z "$cmddev" ]; then
		  echo "There are more choices ($ovr). You must select right device to erase."
		  echo "beesip factory-defaults device"
		  return 1
		else
		  if echo "$ovr" | grep -q "$cmddev" && [ -b "$cmddev" ]; then
		    (echo 'BeeSipData'; dd if=/dev/zero bs=1M count=1) >$cmddev && reboot -f
		  else
		    echo "Error! Given device ($cmddev) is not listed in kernel overlay option ($ovr) or is not block device! Exiting."
		    return 1
		  fi
		fi
	else
		yes | head -3 | jffs2reset && reboot -f
	fi
}

# $1 IP or DNS of gateway
# $2 description
beesip_add_trusted_gw() {
	sqlite3 /etc/kamailio/kamailio.db <<EOF
INSERT INTO dr_gateways (type,address,description) VALUES(5,'$1','$2');
EOF
}

# $1 IP or DNS of gateway
# $2 description
beesip_add_external_gw() {
	sqlite3 /etc/kamailio/kamailio.db <<EOF
INSERT INTO dr_gateways (type,address,description) VALUES(1,'$1','$2');
EOF
}

# $1 prefix
# $2 IP or DNS of gateway
# $3 description
beesip_add_local_prefix() {
	sqlite3 /etc/kamailio/kamailio.db <<EOF
INSERT INTO dr_rules (groupid,prefix,timerec,routeid,gwlist,description) VALUES('2','$1','20000101T083000',0,(SELECT gwid from dr_gateways WHERE address='$2'),'$3');
EOF
}

beesip_show_gws() {
	echo "Local gaeways"
	sqlite3 /etc/kamailio/kamailio.db "SELECT gwid,address,description from dr_gateways WHERE type=5"
	echo "Remote gateways"
	sqlite3 /etc/kamailio/kamailio.db "SELECT gwid,address,description from dr_gateways WHERE type=2"
}

beesip_show_prefixes() {
	sqlite3 /etc/kamailio/kamailio.db "SELECT prefix,(SELECT address FROM dr_gateways WHERE gwid=gwlist) FROM dr_rules"
}

beesip_del_gateway() {
	sqlite3 /etc/kamailio/kamailio.db "DELETE FROM dr_gateways WHERE address='$1'"
}

beesip_del_prefix() {
	sqlite3 /etc/kamailio/kamailio.db "DELETE FROM dr_rules WHERE prefix='$1'"
}

beesip_sysupgrade() {
	if [ -z "$1" ]; then
		config_load beesip
		config_get base_uri "global" base_uri
		config_get sysupgrade_img "global" sysupgrade_img
		if [ -n "$base_uri" ] && [ -n "$sysupgrade_img" ]; then
			cd /tmp && curl --capath /etc/ssl/certs -f -s -L -o "${sysupgrade_img}" "${base_uri}/${sysupgrade_img}" && syupgrade ${sysupgrade_img}
		else
			echo "ERROR: Please, provide path to image file in config file or enter url."
		fi
	else
		if [ -f "$1" ]; then
			sysupgrade -v "$1"
		else
		  if [ -n "$1" ]; then
			curl --capath /etc/ssl/certs -f -s -L -o /tmp/sysupgrade.img "$1"
			sysupgrade -v /tmp/sysupgrade.img
		  else
			echo "ERROR: Upgrade Beesip image at $1 not found."
		  fi
		fi
	fi
}

beesip_prepare_kamailiodb() {
  ! [ -f /etc/kamailio/kamctlrc ] && return 1
  . /etc/kamailio/kamctlrc 
  (echo y; echo y) |kamdbctl create
	sqlite3 $DB_PATH <<EOF
  ALTER TABLE acc ADD COLUMN src_user VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE acc ADD COLUMN src_domain VARCHAR(128) NOT NULL DEFAULT '';
  ALTER TABLE acc ADD COLUMN dst_ouser VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE acc ADD COLUMN dst_user VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE acc ADD COLUMN dst_domain VARCHAR(128) NOT NULL DEFAULT '';
  ALTER TABLE acc ADD COLUMN src_ip VARCHAR(128) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN src_user VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN src_domain VARCHAR(128) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN dst_ouser VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN dst_user VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN dst_domain VARCHAR(128) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN src_ip VARCHAR(128) NOT NULL DEFAULT '';

CREATE TABLE dr_gateways (
    gwid INTEGER PRIMARY KEY NOT NULL,
    type INTEGER DEFAULT 0 NOT NULL,
    address VARCHAR(128) NOT NULL,
    strip INTEGER DEFAULT 0 NOT NULL,
    pri_prefix VARCHAR(64) DEFAULT NULL,
    attrs VARCHAR(255) DEFAULT NULL,
    description VARCHAR(128) DEFAULT '' NOT NULL
);
CREATE TABLE dr_rules (
    ruleid INTEGER PRIMARY KEY NOT NULL,
    groupid VARCHAR(255) NOT NULL,
    prefix VARCHAR(64) NOT NULL,
    timerec VARCHAR(255) NOT NULL,
    priority INTEGER DEFAULT 0 NOT NULL,
    routeid VARCHAR(64) NOT NULL,
    gwlist VARCHAR(255) NOT NULL,
    description VARCHAR(128) DEFAULT '' NOT NULL
);
CREATE TABLE dr_groups (
    id INTEGER PRIMARY KEY NOT NULL,
    username VARCHAR(64) NOT NULL,
    domain VARCHAR(128) DEFAULT '' NOT NULL,
    groupid INTEGER DEFAULT 0 NOT NULL,
    description VARCHAR(128) DEFAULT '' NOT NULL
);
CREATE TABLE dr_gw_lists (
    id INTEGER PRIMARY KEY NOT NULL,
    gwlist VARCHAR(255) NOT NULL,
    description VARCHAR(128) DEFAULT '' NOT NULL
);
EOF
  [ -n "$SIPTRACEDB" ] && sqlite3 $SIPTRACEDB <<EOF
  CREATE TABLE sip_trace (
    id INTEGER PRIMARY KEY NOT NULL,
    time_stamp TIMESTAMP WITHOUT TIME ZONE DEFAULT '1900-01-01 00:00:01' NOT NULL,
    time_us INTEGER DEFAULT 0 NOT NULL,
    callid VARCHAR(255) DEFAULT '' NOT NULL,
    traced_user VARCHAR(128) DEFAULT '' NOT NULL,
    msg TEXT NOT NULL,
    method VARCHAR(50) DEFAULT '' NOT NULL,
    status VARCHAR(128) DEFAULT '' NOT NULL,
    fromip VARCHAR(50) DEFAULT '' NOT NULL,
    toip VARCHAR(50) DEFAULT '' NOT NULL,
    fromtag VARCHAR(64) DEFAULT '' NOT NULL,
    direction VARCHAR(4) DEFAULT '' NOT NULL
)
EOF
}

#
# Prepare BeeSIP after clean factory defaults
#

beesip_prepare() {
	blog "Preparing Beesip ..."
	ssl_rehash
	beesip_prepare_kamailiodb
	beesip_import_gateways
}

beesip_init() {
        local enabled
        local prepared
	local crashreport
	config_load beesip
        config_get_bool enabled "global" enable 1
	config_get_bool prepared "global" prepared 0
	config_get_bool crashreport "global" crashreport 0

        [ "${enabled}" -eq 0 ] && { echo "Beesip disabled"; return 1; }
        ([ "${prepared}" -eq 0 ] && beesip_prepare && uci set beesip.beesip.prepared=1 && uci commit) &
	
	blog "Great init of Beesip"
}

