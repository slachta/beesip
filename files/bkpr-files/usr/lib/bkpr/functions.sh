add_to_cron_if_not_exists(){
	if ! [ -f /etc/crontabs/root ]; then
		echo "BKPRsender: Copying crontab file among crontabs files"
		cp /usr/share/bkprsender/crontab /etc/crontabs/root
	fi

	/etc/init.d/cron start
	/etc/init.d/cron enable
}

#add_firewall_rules(){
#	/etc/init.d/firewall stop
#	iptables-restore < /usr/share/bkprsender/firewall
#}

init(){
        config_load bkprsender
        config_get bkpr_key "common" "key"
        config_get bkpr_name "common" "name"
        config_get bkpr_server_address "common" "server_addr"
        config_get bkpr_workdir "common" "workdir"

	add_to_cron_if_not_exists
	#set_firewall_rules

	KEY=$bkpr_key
        NAME=$bkpr_name
        BKPRADDR=$bkpr_server_address
        # other configuration
        NODEIP=$(ip a s eth0 | grep -o -E "(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])" | head -n 1)
        BEEKEEPERURL=http://$BKPRADDR:8080/beekeeper/stcpdupload
        WORKDIR=$bkpr_workdir
        DONEDIR=$WORKDIR/done
        PENDIR=$WORKDIR/pending
        FAILDIR=$WORKDIR/failed
        FILES=$WORKDIR/*.pcap
        RESULT=$WORKDIR/tcpd.csv
        TEMP=$WORKDIR/temp.txt
        ITEMS=$(ls -l $WORKDIR | grep pcap | wc -l)
        LASTFILEMOD=""
}

bkpr_help(){
	init
	cat <<EOF

  Beesip BKPRnode commandline utility.

  Usage:
    bkprsender help
	- This command prints help for bkprnode command.

    bkprsender pack
	- This command packs the pcap data.

    bkprsender upload
	- This command prepares and uploads the packaged data to beekeeper server.

EOF

	echo -e "  BKPR node variables are:\n"
        echo -e "\tkey variable:   "$bkpr_key
        echo -e "\tname variable:  "$bkpr_name
        echo -e "\tserver address: "$bkpr_server_address
        echo -e "\tworking dir:    "$bkpr_workdir"\n"
}

bkpr_pack(){
	init
	if ls $DONEDIR* 1> /dev/null 2>&1; then
		echo "BKPRsender: pcap files do exist"
		tar cfz $DONEDIR/sip$(date +%F).tar.gz $DONEDIR/*.pcap
		rm $DONEDIR/*.pcap
	else
		echo "BKPRsender: pcap files do not exist"
	fi
}

bkpr_upload(){
        # If there is only one pcap file in trace folder, exit

        if [ "$ITEMS" -lt "2" ]; then
		echo "No suitable pcap files found, exiting."
		exit
        fi

        # Check folder structure

        if [ ! -d $WORKDIR ]; then
                mkdir -p $WORKDIR
        fi
        if [ ! -d $DONEDIR ]; then
                mkdir $DONEDIR
        fi
        if [ ! -d $PENDIR ]; then
                mkdir $PENDIR
        fi
        if [ ! -d $FAILDIR ]; then
                mkdir $FAILDIR
        fi

        echo "$ITEMS pcaps found, running csv data extraction."
        echo $(date)

        cd $WORKDIR
        if [ -f $RESULT ]; then
            echo "$RESULT exists! Deleting ..."
            rm $RESULT
            sleep 5
        fi

        # Process all pcaps with some data
        for f in $FILES
        do
          if [ -s $f ]; then
            /usr/sbin/tcpdump -ttttnnr $f -v > $TEMP
            /usr/lib/bkpr/parse.py $TEMP $NODEIP >> $RESULT
            LASTFILEMOD=$(date -r $f '+%F %T')
            #echo $LASTFILEMOD
            mv $f $PENDIR
            rm $TEMP
          fi
        done

        echo "Files processed and moved to pending folder. Uploading ..."

        # Finally send tcpd csv to beekeeper
        UPLOADRESULT=$(curl -F "key=$KEY" -F "hostname=$NAME" -F "csvfile=@$RESULT" -F "endTime=$LASTFILEMOD" $BEEKEEPERURL)
        echo "upload result: $UPLOADRESULT"
        if [[ "$UPLOADRESULT" == "OK" ]]; then
          echo "Deleting uploaded csv, moving files to done folder."
          rm $RESULT
          mv $PENDIR/*.pcap $DONEDIR
        else
          echo "Moving pending files to failed folder."
          mv $PENDIR/*.pcap $FAILDIR
        fi
        echo
}
